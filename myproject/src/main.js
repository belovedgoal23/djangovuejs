/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
//require('./bootstrap');
//require('jquery-lazy');
//require('jquery-validation');
//import swal from 'sweetalert';
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App';
import Home from './components/HomeComponent'
import About from './components/AboutComponent'
import pageNotFound from './components/NotFoundComponent'
//window.Vue = require('vue');
Vue.use(VueRouter);
// new Vue({
//   el: '#app',
//   template: '<App/>',
//   data : {
//     name : 'ayanfe',
//     femi : 'goal'
//   },
//   components: { App }

// });

const routes = [
    { path: '/', component: Home, name: 'Home'},
    { path: '/about', component: About, name: 'About' },
    {path: "*", component:pageNotFound}
  ]

const router = new VueRouter({
    routes // short for `routes: routes`
  })
  
const app = new Vue({
    router
  }).$mount('#app')

