from myproject.settings import INSTALLED_APPS, ALLOWED_HOSTS, BASE_DIR, DEBUG
import os

INSTALLED_APPS.append('webpack_loader',)

WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': not DEBUG,
        'BUNDLE_DIR_NAME': os.path.join(BASE_DIR, 'myproject'),
        'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats.json'),
        'POLL_INTERVAL': 0.1,
        'TIMEOUT': None,
        'IGNORE': ['.+\.hot-update.js', '.+\.map']
    }
}
print WEBPACK_LOADER

#/Users/oluwasemilore/projects/myproject/myproject/webpack-stats.json