#!/bin/bash
#source ~/.virtualenvs/django10/bin/activate
exec gunicorn myproject.wsgi -b 0.0.0.0:9006 \
     --workers=5 -t 2300 -k gevent \
     --worker-connections 100