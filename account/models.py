from django.core.signing import Signer
from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
     user = models.OneToOneField(User, null=False, blank=False, related_name='user_profile')
     phone = models.CharField(max_length=15, null=True, unique=True, blank=True)

     def __unicode__(self):
         return self.user.username
