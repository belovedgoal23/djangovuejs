from django.shortcuts import render
from django.views.generic import FormView
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.views.decorators.cache import never_cache
#from models import UserProfile
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from myproject.helper import is_verified_required
from django.contrib.auth.models import User
from .forms import UserLoginForm #CompleteProfileForm
import json
from django.http import HttpResponse
# Create your views here.


def handler404(request):
    response = render_to_response('account/404.html', {},
                                  RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('account/500.html', {},
                                  RequestContext(request))
    response.status_code = 500
    return response


def logout(request):
    auth_logout(request)
    response = redirect('account:login')
    return response


# @never_cache
# def home(request):
#     return redirect('account:login')

@never_cache
def home(request, template='account/home.html'):
    context = {}
    return render(request, template, context)


# @login_required
# def dashboard(request, template='account/dashboard.html'):
#     travel = Travel.objects.all()
#     road_count = travel.filter(user=request.user,trip_type='Road Trip').count()
#     one_way = travel.filter(user=request.user, trip_type='One Way Flight').count()
#     return_f = travel.filter(user=request.user, trip_type='Return Flight').count()
#     multiple = travel.filter(user=request.user, trip_type='Multiple Flight').count()
#     context = {'road_count': road_count, 'one_way': one_way, 'return_f': return_f, 'multiple': multiple}
#     return render(request, template, context)
    

class LoginView(FormView):
    template_name = 'account/loginn.html'
    form_class = UserLoginForm
    success_url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(self.success_url)
        return super(LoginView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        #import pdb
        #pdb.set_trace()
        auth_login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)
